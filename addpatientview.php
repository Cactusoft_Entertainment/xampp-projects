<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/reset.css">
    <link rel="stylesheet" href="style/style.css">
    <title>Ptacki.net - dodaj pacjenta</title>
</head>
<body class="background">
    <div class="container">
        <?php require("php/nav.php") ?>
        <header class="header-main background-normal"><h1>Dodaj pacjenta</h1></header>
        <div class="addrecord-container background-gray">
            <div>
                <span>Gatunek</span>
                <span>Data przyjęcia</span>
                <span>Data wypisania</span>
                <span>Stan pacjenta</span>
                <span>Opis</span>
            </div>
            <div>
                <span class="hint-list"><input type="text" name="spec" required><ul id="search"></ul></span>
                <span><input type="date" name="add_date" required></span>
                <span><input type="date" name="end_date"></span>
                <span><select name="state">
                    <option value='1'>pozostaje na rehabilitacji</option>
                    <option value='2'>wypuszczony</option>
                    <option value='3'>upadek</option>
                    <option value='4'>eutanazja</option>
                    <option value='5'>przeznaczony do azylu</option>
                <select></span>
                <textarea name="descr" rows="2"></textarea>
            </div>
            <p id="info"></p>
            <p><input type="button" class="button background-normal background-normal-hover" onclick="Add()" value="Dodaj pacjenta"></p>
        </div>
    </div>
</body>
<script>
    let search = document.getElementById("search");
    let specInput = document.getElementsByName("spec")[0];
    let addDateInput = document.getElementsByName("add_date")[0];
    let endDateInput = document.getElementsByName("end_date")[0];
    let stateInput = document.getElementsByName("state")[0];
    let descrInput = document.getElementsByName("descr")[0];
    let info = document.getElementById("info");
    let specId;

    specInput.addEventListener("input", function() {
        let x = specInput.value;
        let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    search.innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET", "php/specSearch.php?phr=" + x, true);
            xmlhttp.send();
    });

    function writeSpec(li, id) {
        specInput.value = li.innerHTML;
        specId = id;
        while(search.firstChild)
        {
            search.removeChild(search.firstChild);
        }
    }

    function Add()
    {
        //console.log(specId+", "+addDateInput.value+", "+endDateInput.value+", "+stateInput.value+", "+descrInput.value);
        if(specInput.value!="" && specId!=null && addDateInput!="")
        {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    info.innerHTML = this.responseText;
                    setTimeout(() => {
                        info.innerHTML = "";
                    }, 2000);
                }
                else {
                    info.innerHTML = "<p class='warnings'>Błąd przy dodawaniu pacjenta</p>";
                }
            };
            xmlhttp.open("GET", "php/addRecord.php?t=patients&sp=" + specId + "&a=" + addDateInput.value + "&e=" + endDateInput.value + "&st=" + stateInput.value + "&d=" + descrInput.value.toLowerCase(), true);
            xmlhttp.send();
        }
        else
        {
            info.innerHTML = "Proszę uzupełnić wymagane pola";
            setTimeout(() => {
                info.innerHTML = "";
            }, 2000);
        }
    }
</script>
</html>