<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/reset.css">
    <link rel="stylesheet" href="style/style.css">
    <title>Ptacki.net - lista pacjentów</title>
</head>
<body class="background">
    <div class="container">
        <?php require("php/nav.php") ?>
        <header class="header-main background-normal"><h1>Tabela</h1></header>
        <div class="table-container background-gray">
            <table class="table no-font-scale">
                <?php require("php/table.php") ?>
            </table>
            <span>
            <hr>
            Ilość wyświetlanych wpisów: <input type="number" name="n">
            </span>
        </div>
    </div>
    <div id="edit-window" class="background-green">
        <h3>Edycja</h3>
        <div id="edit-window-input"></div>
        <input type="button" class="button background-gray background-gray-hover" onclick="closeWindow()" value="Anuluj">
        <input type="button" class="button background-normal background-normal-hover" onclick="submitEdit()" value="OK">
        <script src="js/editWindow.js"></script>
    <div>
</body>
<script>
    let table = document.getElementsByTagName("table")[0];
    let nButton = document.getElementsByName("n")[0];
    let order = "addmission_date";
    let dir = "DESC";

    document.getElementsByTagName("h1")[0].innerHTML = (editTable=="patients")? "Lista pacjentów" : "Lista gatunków";
    document.getElementsByName("n")[0].value = n;

    nButton.addEventListener("change", function () {
        if(nButton.value > 0)
        {
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    table.innerHTML = this.responseText;
                }
            };
            n = nButton.value;
            xmlhttp.open("GET", "php/table.php?t=" + editTable + "&n=" + n + "&o=" + order + "&d=" + dir, true);
            xmlhttp.send();
            
        }
        else
        {
            nButton.value = 1;
        }
    });

    function sort(el,o)
    {
        if(dir=="ASC")
        {
            dir = "DESC";
            order = o;
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    table.innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET", "php/table.php?t=" + editTable + "&n=" + n + "&o=" + order + "&d=" + dir, true);
            xmlhttp.send();
        }
        else
        {
            dir = "ASC";
            order = o;
            let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    table.innerHTML = this.responseText;
                }
            };
            xmlhttp.open("GET", "php/table.php?t=" + editTable + "&n=" + n + "&o=" + order + "&d=" + dir, true);
            xmlhttp.send();
        }
    }
</script>
</html>