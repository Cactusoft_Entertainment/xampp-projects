<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/reset.css">
    <link rel="stylesheet" href="style/style.css">
    <title>Ptacki.net - dodaj gatunek</title>
</head>
<body class="background">
    <div class="container">
        <?php require("php/nav.php") ?>
        <header class="header-main background-normal"><h1>Dodaj gatunek</h1></header>
        <div class="addrecord-container background-gray">
            <div>
                <span>Łacińska nazwa gatunku</span>
                <span>Polska nazwa gatunku</span>
            </div>
            <div>
                <span><input type="text" name="latin" required></span>
                <span><input type="text" name="polish"></span>
            </div>
            <p id="info"></p>
            <p><input type="button" class="button background-normal background-normal-hover" onclick="Add()" value="Dodaj gatunek"></p>
        </div>
    </div>
</body>
<script>
    let latinInput = document.getElementsByName("latin")[0];
    let polishInput = document.getElementsByName("polish")[0];

    function Add()
    {
        //console.log(specId+", "+addDateInput.value+", "+endDateInput.value+", "+stateInput.value+", "+descrInput.value);
        let xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    info.innerHTML = this.responseText;
                    setTimeout(() => {
                        info.innerHTML = "";
                    }, 2000);
                }
                else {
                    info.innerHTML = "<p class='warnings'>Błąd przy dodawaniu pacjenta</p>";
                }
            };
            xmlhttp.open("GET", "php/addRecord.php?t=species&lat=" + latinInput.value.toLowerCase() + "&pol=" + polishInput.value.toLowerCase(), true);
            xmlhttp.send();
    }
</script>
</html>