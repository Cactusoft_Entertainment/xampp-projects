<?php
    if(isset($_COOKIE["surname"]))
    {
        setcookie("surname", $_COOKIE["surname"], time() + 300, "/");
        setcookie("name", $_COOKIE["name"], time() + 300, "/");
    }
    else
    {
        header("Location: index.php?i=1");
    }
?>
<nav class="nav-main">
    <button class="button background-green background-green-hover" onclick="HideNav()">...</button>
    <div class="nav-main-hiding">
        <div class="user-id">
            <span><?php echo ucfirst($_COOKIE["name"]); ?></span>
            <span><?php echo ucfirst($_COOKIE["surname"]); ?></span>
        </div>
        <ul>
            <a href="tableview.php?t=patients&o=addmission_date"><li>Ostatni pacjenci</li></a>
            <a href="addpatientview.php"><li>Dodaj pacjenta</li></a>
            <a href="addspeciesview.php"><li>Dodaj gatunek</li></a>
            <a href="tableview.php?t=species&n=50&o=latin&d=ASC"><li>Lista gatunków</li></a>
            <a href=""><li>Raport</li></a>
        </ul>
    <div>
</nav>
<script>
    let nav = document.getElementsByClassName("nav-main-hiding")[0];
    function HideNav(){
        if(nav.style.display=="none" || nav.style.display=="")
        {
            nav.style.display = "block";
        }
        else
        {
            nav.style.display = "none";
        }
    }
</script>