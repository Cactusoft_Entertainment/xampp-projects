<?php
    if(isset($_COOKIE["surname"]))
    {
        setcookie("surname", $_COOKIE["surname"], time() + 300, "/");
        setcookie("name", $_COOKIE["name"], time() + 300, "/");
    }
    else
    {
        header("Location: index.php?i=1");
    }

    if(@$conn = mysqli_connect("localhost","root","","ptacki"))
    {
        mysqli_query($conn,"SET NAMES utf8");
        $table = (isset($_REQUEST["t"]))? $_REQUEST["t"] : "patients";
        $n = (isset($_REQUEST["n"]))? $_REQUEST["n"] : 10;
        $order = (isset($_REQUEST["o"]))? $_REQUEST["o"] : "id";
        $dir = (isset($_REQUEST["d"]))? $_REQUEST["d"] : "DESC";

        $query = ($table=="patients")? "SELECT patients.id,polish,addmission_date,end_of_treatment_date,state,description FROM patients,species WHERE species_id=species.id ORDER BY ".$order." ".$dir." LIMIT ".$n :  "SELECT id,latin,polish FROM species ORDER BY ".$order." ".$dir." LIMIT ".$n;
        if($query)
        {
            $q1 = mysqli_query($conn, $query);

            $headers = array(
                "latin" => "Łacińska nazwa gatunku",
                "polish" => "Polska nazwa gatunku",
                "addmission_date" => "Data przyjęcia",
                "end_of_treatment_date" => "Data wypisania",
                "state" => "Stan pacjenta",
                "description" => "Opis",
                "name" => "Imie",
                "surname" => "Nazwisko"
            );
            $options = array(
                "index 0",
                "pozostaje na rehabilitacji",
                "wypuszczony",
                "upadek",
                "eutanazja",
                "przeznaczony do azylu"
            );

            $arr1 = mysqli_fetch_assoc($q1);
            foreach($arr1 as $k => $x)
            {
                echo ($k=="latin")? "<th class='table-header' onclick='sort(this,\"latin\")'>".$headers["latin"]."</th>" : "";
                echo ($k=="polish")? "<th class='table-header' onclick='sort(this,\"polish\")'>".$headers["polish"]."</th>" : "";
                echo ($k=="addmission_date")? "<th class='table-header' onclick='sort(this,\"addmission_date\")'>".$headers["addmission_date"]."</th>" : "";
                echo ($k=="end_of_treatment_date")? "<th class='table-header' onclick='sort(this,\"end_of_treatment_date\")'>".$headers["end_of_treatment_date"]."</th>" : "";
                echo ($k=="state")? "<th class='table-header' onclick='sort(this,\"state\")'>".$headers["state"]."</th>" : "";
                echo ($k=="description")? "<th class='table-header' onclick='sort(this,\"description\")'>".$headers["description"]."</th>" : "";
                echo ($k=="name")? "<th>".$headers["name"]."</th>" : "";
                echo ($k=="surname")? "<th>".$headers["surname"]."</th>" : "";
            }

            $q2 = mysqli_query($conn, $query);
            for($i = 0; $i<mysqli_num_rows($q2); $i++)
            {
                echo "<tr>";
                $arr2 = mysqli_fetch_assoc($q2);
                $id;
                foreach($arr2 as $k => $x)
                {
                    if($k=="id")
                    {
                        $id=$x;
                    }
                    else
                    {
                        echo ($k=="state")? "<td class='center'>".$options[$x]."</td>" : "<td>".$x."</td>";
                    }
                }
                echo "<td><select><option onclick=viewEdit('$table',".$id.")>Edytuj</option><option class='warnings' onclick=deleteRecord('$table',".$id.")>Usuń</option></select></td>";
                echo "</tr>";
            }
            echo "<script>let editTable='$table'; let n = ".$n.";</script>";
        }
        else
        {
            echo "<p class='warnings'>Błąd zapytania SQL</p>";
        }
    }
    else
    {
        echo "<p class='warnings'>Błąd połączenia z bazą danych</p>";
    }
?>