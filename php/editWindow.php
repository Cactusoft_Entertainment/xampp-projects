<?php
    if(@$conn = mysqli_connect("localhost","root","","ptacki"))
    {
        mysqli_query($conn,"SET NAMES utf8");
        $id = $_REQUEST["id"];
        $table = $_REQUEST["t"];
        if($table == "patients")
        {
            $q1 = mysqli_query($conn,"SELECT species_id,polish,addmission_date,end_of_treatment_date,state,description FROM patients,species WHERE species_id=species.id AND patients.id=".$id);
            $arr = mysqli_fetch_assoc($q1);
            $options = array(
                "index 0",
                "<option value='1'>pozostaje na rehabilitacji</option>",
                "<option value='2'>wypuszczony</option>",
                "<option value='3'>upadek</option>",
                "<option value='4'>eutanazja</option>",
                "<option value='5'>przeznaczony do azylu</option>"
            );
            //$id = 0;
            foreach($arr as $k => $x)
            {
                switch($k)
                {
                    case "species_id":
                        echo "<input id='spec_id' style='display:none;' value='$x'>";
                    break;
                    case "polish":
                        echo "<span class='hint-list'><input type='text' name='spec' value='$x' required><ul id='search'></ul></span>";
                    break;
                    case "addmission_date":
                        echo "<input type='date' name='add_date' value='$x'>";
                    break;
                    case "end_of_treatment_date":
                        echo "<input type='date' name='end_date' value='$x'>";
                    break;
                    case "state":
                        echo "<select name='state'>";
                        echo $options[$x];
                        echo ($x!=1)? $options[1] : "";
                        echo ($x!=2)? $options[2] : "";
                        echo ($x!=3)? $options[3] : "";
                        echo ($x!=4)? $options[4] : "";
                        echo ($x!=5)? $options[5] : "";
                        echo "</select>";
                    break;
                    case "description":
                        echo "<textarea name='descr'>".$x."</textarea>";
                    break;
                    default:
                        echo "<input type='text' name='' value='$x'>";
                    break;
                }
            }
        }
        if($table == "species")
        {
            $q1 = mysqli_query($conn,"SELECT latin,polish FROM species WHERE id=".$id);
            $arr = mysqli_fetch_assoc($q1);
            foreach($arr as $k => $x)
            {
                switch($k)
                {
                    case "latin":
                        echo "<input name='lat' value='$x'>";
                    break;
                    case "polish":
                        echo "<input name='pol' value='$x'>";
                    break;
                    default:
                        echo "<input type='text' name='' value='$x'>";
                    break;
                }
            }
        }
    }
    else
    {
        echo "<p class='warnings'>Błąd połączenia z bazą danych</p>";
    }
    /*
    1 - pozostający na rehabilitacji
    2 - wypuszczony
    3 - upadek
    4 - eutanazja
    5 - przeznaczony do azylu
    */
?>