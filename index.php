<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/reset.css">
    <link rel="stylesheet" href="style/style.css">
    <title>Ptacki.net - logowanie</title>
</head>
<body class="background">
    <div class="container flex-horizontal-center">
        <div class="tile-login background-gray">
            <form method="POST">
                <div>
                    <span>Nazwisko:</span>
                    <input type="text" name="surname" required>
                </div>
                
                <div>
                    <span>Hasło:</span>
                    <input type="password" name="password" required>
                </div>
                <div class="tile-login-buttons">
                    <input type="submit" class="button background-normal" value="Logowanie">
                </div>
                <p class="warnings"><?php require("php/login.php"); ?></p>
            </form>
        </div>
    </div>
</body>
</html>