let editWindow = document.getElementById("edit-window");
let editInput = document.getElementById("edit-window-input");
let editId;
let specId;
let specInput;
let search;

function viewEdit(t, id)
{
    editWindow.style.display = "block";

    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            editInput.innerHTML = this.responseText;
            if(t=="patients")
            {
                specId = specId = document.getElementById("spec_id").value;
                specInput = document.getElementsByName("spec")[0];
                search = document.getElementById("search");

                specInput.addEventListener("input", function() {
                    let x = specInput.value;
                    let xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            search.innerHTML = this.responseText;
                        }
                    };
                    xmlhttp.open("GET", "php/specSearch.php?phr=" + x, true);
                    xmlhttp.send();
                });
            }
        }
    };
    xmlhttp.open("GET", "php/editWindow.php?t=" + t + "&id=" + id, true);
    xmlhttp.send();
    editTable = t;
    editId = id;
}

function writeSpec(li, id) {
    specInput.value = li.innerHTML;
    specId = id;
    while(search.firstChild)
    {
        search.removeChild(search.firstChild);
    }
    console.log(specId+" "+editTable);
}

function submitEdit()
{
    if(editTable=="patients")
    {
        let date1 = document.getElementsByName("add_date")[0].value;
        let date2 = document.getElementsByName("end_date")[0].value;
        let state = document.getElementsByName("state")[0].value;
        let descr = document.getElementsByName("descr")[0].value;
        
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //odświeżenie tabeli
                let xmlhttp2 = new XMLHttpRequest();
                xmlhttp2.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                    table.innerHTML = this.responseText;
                    }
                };
                xmlhttp2.open("GET", "php/table.php?t=" + editTable + "&n=" + n + "&o=" + order + "&d=" + dir, true);
                xmlhttp2.send();

                editWindow.style.display = "none";
            }
        };
        xmlhttp.open("GET", "php/submitEdit.php?t=" + editTable + "&id=" + editId + "&spec=" + specId + "&add=" + date1 + "&end=" + date2 + "&state=" + state + "&descr=" + descr, true);
        xmlhttp.send();
    }
    if(editTable=="species")
    {
        let lat = document.getElementsByName("lat")[0].value;
        let pol = document.getElementsByName("pol")[0].value;

        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                //odświeżenie tabeli
                let xmlhttp2 = new XMLHttpRequest();
                xmlhttp2.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        table.innerHTML = this.responseText;
                    }
                };
                xmlhttp2.open("GET", "php/table.php?t=" + editTable + "&n=" + n + "&o=id&d=" + dir, true);
                xmlhttp2.send();
            }
        };
        xmlhttp.open("GET", "php/submitEdit.php?t=" + editTable + "&id=" + editId + "&lat=" + lat + "&pol=" + pol, true);
        xmlhttp.send();
        editWindow.style.display = "none";
    }
}

function deleteRecord(t, id)
{
    if(confirm("Napewno chcesz usunąć ten rekord?"))
    {
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                let xmlhttp2 = new XMLHttpRequest();
                xmlhttp2.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        table.innerHTML = this.responseText;
                    }
                };
                if(editTable=="patients")
                {
                    xmlhttp2.open("GET", "php/table.php?t=" + t + "&n=" + n + "&o=" + order + "&d=" + dir, true);
                    xmlhttp2.send();
                }
                if(editTable=="species")
                {
                    xmlhttp2.open("GET", "php/table.php?t=" + t + "&n=" + n + "&o=id&d=" + dir, true);
                    xmlhttp2.send();
                }
            }
        };
        xmlhttp.open("GET", "php/deleteRecord.php?t=" + t + "&id=" + id, true);
        xmlhttp.send();
    }
}

function closeWindow()
{
    editWindow.style.display = "none";
}