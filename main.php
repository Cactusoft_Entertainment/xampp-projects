<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/reset.css">
    <link rel="stylesheet" href="style/style.css">
    <title>Ptacki.net - menu</title>
</head>
<body class="background">
    <div class="container">
        <?php require("php/nav.php") ?>
        <header class="header-main background-normal"><h1>Menu</h1></header>
        <main class="tiles">
            <div class="tile-double">
            <div class="tile hover-grow background-blue">
                    <a href="tableview.php?t=patients&o=addmission_date"><div>+</div><h3>Ostatni pacjenci</h3></a>
                </div>
            </div>
            <div class="tile-single">
                <div class="tile hover-grow background-green">
                    <a href="addpatientview.php"><div>+</div><h3>Dodaj pacjenta</h3></a>
                </div>
            </div>
            <div class="tile-single">
                <div class="tile hover-grow background-purple">
                    <a href="addspeciesview.php"><div>+</div><h3>Dodaj gatunek</h3></a>
                </div>
            </div>
            <div class="tile-single">
                <div class="tile hover-grow background-orange">
                    <a href="tableview.php?t=species&n=50&o=latin&d=ASC"><div>+</div><h3>Lista gatunków</h3></a>
                </div>
            </div>
            <div class="tile-single">
                <div class="tile hover-grow background-blue">
                    <a href=""><div>+</div><h3>Raport</h3></a>
                </div>
            </div>
        </main>
    </div>
</body>
</html>